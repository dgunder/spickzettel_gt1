\select@language {ngerman}
\contentsline {section}{\numberline {1}Assembler}{1}
\contentsline {subsection}{\numberline {1.1}Assembler Befehle}{1}
\contentsline {subsection}{\numberline {1.2}Flags}{2}
\contentsline {subsubsection}{\numberline {1.2.1}Zero Flag}{2}
\contentsline {subsubsection}{\numberline {1.2.2}Negativ Flag}{2}
\contentsline {subsubsection}{\numberline {1.2.3}Carry Flag}{2}
\contentsline {subsubsection}{\numberline {1.2.4}Overflow Flag}{2}
\contentsline {section}{\numberline {2}Negative Zahlen}{2}
\contentsline {subsection}{\numberline {2.1}Betrag mit Vorzeichen}{2}
\contentsline {subsection}{\numberline {2.2}Einer Komplement}{2}
\contentsline {subsection}{\numberline {2.3}Zweier Komplement}{3}
\contentsline {subsection}{\numberline {2.4}Excess-Darstellung}{3}
\contentsline {section}{\numberline {3}IEEE/Kommazahlenumrechnung}{3}
\contentsline {subsection}{\numberline {3.1}Bin\IeC {\"a}r zu Dezimal}{3}
\contentsline {subsection}{\numberline {3.2}Dezimal zu Bin\IeC {\"a}r}{3}
